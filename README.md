# pi-dashcam
A car dash camera built using raspberry pi camera module.

## Ingredients
- One Raspberry pi (I am using a [B+](https://www.raspberrypi.org/products/model-b-plus/) model)
- One Raspberry pi camera module
- A [push-down button](https://www.adafruit.com/product/367) for properly shutting down your pi
- A breadboard
- An LED
- 220ohm resistor
- Some wires

### Optional
- 3mm plywood or acrylic to create a case for your pi and also a platform for mounting it

## Setup
Inside your raspberry pi do these:
```bash
git clone https://github.com/amshali/pi-dashcam
mkdir $HOME/bin
mkdir $HOME/camera
cp pi-dashcam/cleanup.py $HOME/bin/
cp pi-dashcam/vid.py $HOME/bin/
chmod +x $HOME/bin/cleanup.py $HOME/bin/vid.py
sudo cp pi-dashcam/rc.local /etc/
```

## Cleaning up old files
In order to have enough space for camera to operate we need to regularly free up space on disk. `cleanup.py` file checks for the disk space and removes older files to free up enough space.

Add this command to crontab to run this program every 15 min. This command will remove enough files from `$HOME/camera` to free up 1000MB when the free space is below 1000MB.
```bash
*/15 * * * * $HOME/bin/cleanup.py $HOME/camera 1000 1000
```

## Wiring
<img align="right" src="./schematic-wiring.png">
Here is an image which shows the wiring for LED and shutdown button. One side of **pushdown button** must be connected to **GPIO#15**. The **LED** is connected to the **GPIO#14** using a *220ohm* resistor. The other side of LED and pushdown button are grounded. 



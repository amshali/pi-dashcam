#!/usr/bin/python

# This is a cleanup program which is run periodically to make sure we have enpugh 
# space on disk to store video files.

from __future__ import division
import sys
import subprocess
from os import listdir, remove
from os.path import isfile, islink, join, getsize, getmtime

def FreeSpaceMB(delete_dir):
  df = subprocess.Popen(["df", "--output=avail", delete_dir], stdout=subprocess.PIPE)
  output = df.communicate()[0]
  avail = int(output.split("\n")[1])
  # avail is in KB. Convert it to MB:
  return avail / 1024

def Cleanup(delete_dir, delete_threshold, freeup_amount):
  free_space = FreeSpaceMB(delete_dir)
  if free_space < delete_threshold:
    files = [f for f in map(lambda x: join(delete_dir, x), listdir(delete_dir)) \
      if isfile(f) and not islink(f)]
    # Sort files acsending based on their modification time.
    files.sort(key=lambda f: getmtime(f))
    freed = 0.0
    # Delete enough files to free up enough space that macthes freeup_amount
    for f in files:
      # Size of file in MB
      f_size = getsize(f) / 1024 / 1024
      remove(f)
      print "Deleted ", f
      freed = freed + f_size
      if freed >= freeup_amount:
        break

if __name__ == "__main__":
  delete_dir = sys.argv[1]
  # in MB
  delete_threshold = int(sys.argv[2])
  # in MB
  freeup_amount = int(sys.argv[3])
  Cleanup(delete_dir, delete_threshold, freeup_amount)

